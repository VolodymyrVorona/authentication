import PostModel from '@/resources/post/post.model';
import IPost from '@/resources/post/post.interface';

class PostService {
  private post = PostModel;

  public async create(title: string, body: string): Promise<IPost> {
    try {
      const post = await this.post.create({ title, body });

      return post;
    } catch (error) {
      throw new Error('Unable to create post');
    }
  }
}

export default PostService;
