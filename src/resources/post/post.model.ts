import { Schema, model } from 'mongoose';
import IPost from '@/resources/post/post.interface';

const postSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    body: {
      type: String,
      required: true,
    },
  },
  { timestamps: true },
);

const PostModel = model<IPost>('Post', postSchema);

export default PostModel;
